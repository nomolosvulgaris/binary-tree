package BTree;

import java.util.function.Consumer;

public interface BinaryTreeInterface<T extends Comparable<T>> {
    public BinaryTreeInterface getLeft();
    public BinaryTreeInterface getRight();
    public int getCount();
    public T getValue();
    public void addValue(T value);
    public void forEach(Consumer<T> action);
}

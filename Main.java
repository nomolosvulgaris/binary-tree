package BTree;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        final BinaryTree binaryTree = new BinaryTree(1);
        final Random generator = new Random();
        for (int i = 0; i < 100; i++) {
            new Thread() {
                @Override
                public void run() {
                    binaryTree.addValue(generator.nextInt(11) - 5);
                }
            }.run();
        }
        binaryTree.echo();
    }
}

package BTree;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class BinaryTree<T extends Comparable<T>> implements BinaryTreeInterface<T> {

    private BinaryTree<T> left;
    private BinaryTree<T> right;
    private final T value;
    private AtomicInteger count = new AtomicInteger(1);

    public BinaryTree(T value) {
        this.value = value;
    }

    @Override
    public BinaryTreeInterface getLeft() {
        return left;
    }

    @Override
    public BinaryTreeInterface getRight() {
        return right;
    }

    @Override
    public int getCount() {
        return count.get();
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public void addValue(T value) {
        int spaceship = this.value.compareTo(value);
        if (spaceship < 0) {
            left = addToBranch(left, value);
        } else if (spaceship > 0) {
            right = addToBranch(right, value);
        } else {
            count.incrementAndGet();
        }
    }

    private BinaryTree<T> addToBranch(BinaryTree<T> branch, T value) {
        if (branch == null) {
            branch = new BinaryTree<>(value);
        } else {
            branch.addValue(value);
        }
        return branch;
    }

    @Override
    public void forEach(final Consumer<T> action) {
        forEachPair((value, dummy) -> action.accept(value));
    }

    public void forEachPair(final BiConsumer<T, Integer> action) {
        new Thread("Processing " + getValue()) {
            @Override
            public void run() {
                action.accept(getValue(), getCount());
            }
        }.start();
        if (left != null) {
            left.forEachPair(action);
        }
        if (right != null) {
            right.forEachPair(action);
        }
    }

    public void echo() {
        forEachPair((value, count) -> System.out.println(value + " times " + count));
    }

}